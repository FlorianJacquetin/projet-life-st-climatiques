## Code initié par Seureco dans le cadre du projet Life - Stress-tests climatiques
## Production des graphiques de risques de transition

# Définition de l'espace de travail


# Librairie à charger
library(tidyverse)
library(openxlsx) #create,add,insertPlot,saveWorkbook
library(PBSmodelling) #openFile

## EMISSIONS CO2 LEVEL - ALL MODELS ----

# Data
CO2_All = read.table("data/CO2EM_ALL.csv", header = TRUE, sep = ";")

# Workbook Excel

wb <- createWorkbook()
addWorksheet(wb,"CO2")
addWorksheet(wb,"NZ50")
addWorksheet(wb,"PIB")
addWorksheet(wb,"Sectors")

# Renommer scenarios
CO2_All$Scenario <- as.character(CO2_All$Scenario)
CO2_All$Model[CO2_All$Model == "Imaclim-R"] <- "Imaclim-R France"

CO2_All$Scenario <- fct_relevel(CO2_All$Scenario, 
                                c("Baseline", "NZE 2050", "Divergent",
                                  "Delayed"))

graph_co2 <- ggplot(data=CO2_All, aes(x= Year, y= Value_CO2, colour= Model, shape = Scenario)) +
  theme_bw() +
  theme(legend.title = element_text(size=14),legend.text = element_text(size=12)) +
  geom_line(size=0.5)+
  geom_point(size=1.5)+
  xlab("") +
  labs(colour = "Modèle", shape = "Scénario")+
  scale_y_continuous(name = bquote('Mt/an'),
                     limits=c(0,600), breaks=seq(0,600, by= 100))+
  scale_x_continuous(breaks = seq(from = 2020, to = 2050, by = 2))+
  theme(axis.title.y = element_text(size=12),
        axis.text.x = element_text(size=12, angle =0), axis.text.y = element_text(size=12))

graph_co2

ggsave(filename=paste0("sorties/graph_co2.bmp"),
       plot = graph_co2,
       width = 12, height = 9
       )

insertPlot(wb,"CO2",startRow=2,startCol=2,fileType="bmp", width = 11.5, height = 6)

## GDP and Components deviation NZE 2050 ----

# Données
GDP_Dev_NZE = read.table("data/GDP_Dev_NZE_2050_bis.csv", header = TRUE, sep = ";")


# Renommer scénarios
GDP_Dev_NZE$Scenario <- as.character(GDP_Dev_NZE$Scenario)
GDP_Dev_NZE$Scenario[GDP_Dev_NZE$Scenario == "NZE_2050"] <- "NZE 2050"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "GDP"] <- "PIB"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "Private consumption"] <- "Consommation privée"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "Public consumption"] <- "Consommation publique"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "Investment"] <- "Investissements"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "Exports"] <- "Exportations"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "Imports"] <- "Importations"
GDP_Dev_NZE$GDP_comp[GDP_Dev_NZE$GDP_comp == "Inventories"] <- "Variation de stocks"
GDP_Dev_NZE$Model[GDP_Dev_NZE$Model == "NiGEM_BdF"] <- "NiGEM"
GDP_Dev_NZE$Model[GDP_Dev_NZE$Model == "IMACLIM-R"] <- "Imaclim-R France"

# Ordonne les vecteurs dans le sens voulu
GDP_Dev_NZE$GDP_comp <- fct_relevel(GDP_Dev_NZE$GDP_comp, 
                                    c("PIB", "Consommation privée", "Consommation publique",
                                      "Investissements", "Exports", "Imports", 
                                      "Variation de stocks"))
levels(GDP_Dev_NZE$GDP_comp)

GDP_Dev_NZE$Model <- fct_relevel(GDP_Dev_NZE$Model, 
                                 c("Imaclim-R France", "NEMESIS", "NiGEM", "Three-ME"))
levels(GDP_Dev_NZE$Model)


# Selection de sous-ensemble de données
GDP_Dev_NZE_GDP <- filter(GDP_Dev_NZE, GDP_comp == "PIB")
GDP_Dev_NZE_COMP <- filter(GDP_Dev_NZE, GDP_comp != "PIB", GDP_comp != "Variation de stocks")


# Histogramme de la contribution à la déviation de GDP + Poins GDP
graph_nz50 <- ggplot() +
  theme_bw() +
  theme(legend.title = element_text(size=13),legend.text = element_text(size=11)) +
  geom_col(data=GDP_Dev_NZE_COMP, aes(x= Year, y= Value_dev, fill= GDP_comp))+
  xlab("") +
  facet_grid(rows = vars(Model), scales='free') +
  scale_y_continuous(name = 'Variation en point de pourcentage de PIB (par rapport au scénario "Baseline")',
                     labels = function(x) paste0(x*100, "%"))+
  
  scale_x_continuous(breaks = seq(from = 2020, to = 2050, by = 2))+
  theme(axis.title.x = element_text(size=10), axis.title.y = element_text(size=11), axis.text.x = element_text(size=10), axis.text.y = element_text(size=10)) +
  theme(strip.text.x = element_text(size=10, color="black", face="bold")) +
  theme(strip.text.y = element_text(size=10, color="black", face="bold")) +
  scale_fill_manual(name = "Composantes du PIB", values = c(
    "#F96151", 
    "#CA7DFF", 
    "#E98635",
    "#5897F3", 
    "#93C47D")) +
  geom_line(data=GDP_Dev_NZE_GDP, aes(x= Year, y = Value_dev))+
  geom_point(data=GDP_Dev_NZE_GDP, aes(x= Year, y = Value_dev, shape = "PIB"))+
  labs(shape=NULL)

graph_nz50

insertPlot(wb,"NZ50",startRow=2,startCol=2,fileType="bmp", width = 11.5, height = 6)

## GDP and Components deviation - All Scenarios ----

# Données
GDP_Dev_ALL = read.table("data/GDP_Dev_All_scen_bis.csv", header = TRUE, sep = ";")

# Renommer scénarios
GDP_Dev_ALL$Scenario <- as.character(GDP_Dev_ALL$Scenario)
GDP_Dev_ALL$Scenario[GDP_Dev_ALL$Scenario == "NZE_2050"] <- "NZE2050"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "GDP"] <- "PIB"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "Private consumption"] <- "Consommation privée"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "Public consumption"] <- "Consommation publique"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "Gross fixed capital formation"] <- "Investissements"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "Exports"] <- "Exportations"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "Imports"] <- "Importations"
GDP_Dev_ALL$GDP_comp[GDP_Dev_ALL$GDP_comp == "Inventories"] <- "Variation de stocks"
GDP_Dev_ALL$Model[GDP_Dev_ALL$Model == "NiGEM_BdF"] <- "NiGEM"
GDP_Dev_ALL$Model[GDP_Dev_ALL$Model == "IMACLIM-R"] <- "Imaclim-R France"

# Ordonne les vecteurs dans le sens voulu
GDP_Dev_ALL$GDP_comp <- fct_relevel(GDP_Dev_ALL$GDP_comp, 
                                    c("PIB", "Consommation privée", "Consommation publique",
                                      "Investissements", "Exportations", "Importations", 
                                      "Variation de stocks"))

levels(GDP_Dev_ALL$GDP_comp)

GDP_Dev_ALL$Model <- fct_relevel(GDP_Dev_ALL$Model, 
                                 c("Imaclim-R France", "NEMESIS", "NiGEM", "Three-ME"))
levels(GDP_Dev_ALL$Model)

GDP_Dev_ALL$Scenario <- fct_relevel(GDP_Dev_ALL$Scenario, 
                                    c("NZE2050", "Delayed", "Divergent"))
levels(GDP_Dev_ALL$Scenario)


# Selection de sous-ensemble de donn?es
GDP_Dev_ALL_GDP <- filter(GDP_Dev_ALL, GDP_comp == "PIB")
GDP_Dev_ALL_COMP <- filter(GDP_Dev_ALL, GDP_comp != "PIB", GDP_comp != "Variation de stocks")



graph_all <- ggplot() +
  theme_bw() +
  theme(legend.title = element_text(size=13),legend.text = element_text(size=11)) +
  geom_col(data=GDP_Dev_ALL_COMP, aes(x= Scenario, y= Value_dev, fill= GDP_comp))+
  xlab("") +
  facet_grid(cols = vars(Model)) +
  scale_y_continuous(name = 'Variation moyenne en point de pourcentage de PIB entre 2020 et 2050 \n (par rapport au scénario "Baseline")',
                     labels = function(x) paste0(x*100, "%"), 
                     limits=c(-0.035,0.035), breaks=seq(-0.035,0.035, by= 0.005)) +
  theme(axis.title.x = element_text(size=10), axis.title.y = element_text(size=11), axis.text.x = element_text(size=10), axis.text.y = element_text(size=10)) +
  theme(strip.text.x = element_text(size=11, color="black", face="bold")) +
  scale_fill_manual(name = "Composantes du PIB", values = c(
    "#F96151", 
    "#CA7DFF", 
    "#E98635",
    "#5897F3", 
    "#93C47D"))+
  #geom_line(data=GDP_Dev_ALL_GDP, aes(x= Scenario, y = Value_dev, shape = "PIB"))+
  geom_point(data=GDP_Dev_ALL_GDP, aes(x= Scenario, y = Value_dev, shape = "PIB"))+
  labs(shape=NULL)

graph_all

insertPlot(wb,"PIB",startRow=2,startCol=2,fileType="bmp", width = 11.5, height = 6)

## Sectoral Production - NZ50 ----

# Données
Prod_Dev_ALL = read.table("data/sectors.csv", header = TRUE, sep = ";")
Prod_Dev_ALL$Sector[Prod_Dev_ALL$Sector=="Electricite"] <- "Electricité"
Prod_Dev_ALL$Model[Prod_Dev_ALL$Model=="Modele sectoriel"] <- "Modèle sectoriel"

mycolors <- c("#F96151",
              "#93C47D",
              "#5897F3",
              "#E98635"
              )

graph_sectors <- ggplot() +
  theme_bw() +
  theme(legend.title = element_text(size=14),legend.text = element_text(size=12)) +
  geom_col(data=Prod_Dev_ALL, aes(x= Model, y= Dev,fill=Model))+
  scale_fill_manual(values=mycolors,name = "Modèle")+
  xlab("") +
  facet_grid(cols = vars(Sector)) +
  theme(strip.text = element_text(size = 12)) +
  scale_y_continuous(name = 'Variation de la production sectorielle dans le scénario "NZE 2050" \n (%, en 2050 par rapport au scénario "Baseline")',
                     labels = function(x) paste0(x*100, "%"), 
                     limits=c(-0.8,0.4), breaks=seq(-0.8,0.4, by= 0.2)) +
  theme(axis.title.y = element_text(size=12),
        axis.text.y = element_text(size=13),
        axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()
        )

graph_sectors

insertPlot(wb,"Sectors",startRow=2,startCol=2,fileType="bmp", width = 11.5, height = 6)

saveWorkbook(wb,"sorties/graphiques.xlsx",overwrite = TRUE)

openFile("sorties/graphiques.xlsx")